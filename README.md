FSFTN Map
==========

Introduction
------------
1. This is a demonstration of regions and institutions covered by FSFTN in Tamil Nadu. 
2. Currenlty GLUG's are added. 
3. Institutions, Colleges, GLUG Venues can be added in the future.
4. This would be useful for quick look at the details across Tamil Nadu

Screenshot
------------

![](./images/screenshot.png)

Contributin Details
----------------

1. Gather the Details of the GLUG to be added
2. Get the Latitude and Longitude of the location from the web. Various online services are available.
3. Open the `markers.json` file inside the `data` directory and add those details
4. The Details are added to the map successfully.